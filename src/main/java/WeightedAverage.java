/**
 * 作業00 -- 計算加權平均
 */
public class WeightedAverage {

    /**
     * 回傳加權平均
     *
     * @param grades 成績
     * @param credit 學分數
     * @return 加權平均
     */

    public double solve(int[] grades, int[] credit) {
        // TODO: 請實作作業
        int i;
        float sum=0,total=0,ans=0;
        int a=grades.length;
        int b=credit.length;
        for(i=0;i<a;i++)
        {
            sum+=(float)grades[i]*credit[i];
            total+=credit[i];
        }
        ans=sum/total;
        return ans;
    }

}