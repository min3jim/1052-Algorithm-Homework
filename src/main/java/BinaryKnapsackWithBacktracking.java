import javax.lang.model.element.Element;
import java.lang.reflect.Array;
import java.util.Arrays;

/**
 * 作業03A - 背包問題(Backtracking)
 */
public class BinaryKnapsackWithBacktracking {

    /**
     * 使用 Backtracking 演算法，求出背包可以攜帶最高的攻擊力總和
     *
     * param gemCount          寶石數量
     * param gemsATK           每顆寶石的攻擊力
     * param gemsWeight        每顆寶石的重量
     * param knapsackMaxWeight 背包最高的耐重
     * return 在背包耐重內，最高的寶石攻擊力總和
     */
    private int Gem_Count;      //寶石數量
    private int[] Gem_ATK;      //每顆寶石的攻擊力
    private int[] Gem_Weight;   //每顆寶石的重量
    private int Best_ATK;       //最佳攻擊力
    private int Bag_size;       //包包大小
    private int Now_Weight;     //當前重量
    private int Now_ATK;        //當前攻擊力
    private int[] Gem_in_Bag;   //當前裝入包包順序

    public int solve(int gemCount, int[] gemsATK, int[] gemsWeight, int knapsackMaxWeight) {

        /*初始化各數據*/
        Now_Weight = 0;
        Now_ATK = 0;
        Best_ATK = 0;
        Bag_size = knapsackMaxWeight;
        Gem_Count = gemCount;
        Gem_in_Bag = new int[gemCount+1];
        Element[] gem_CP = new Element[gemCount + 1];
        /*1.先將物品以單位重量(ATK/Weight)進行排序*/
        gem_CP[0] = new Element( 0 , Float.MAX_VALUE );
        for(int i=1;i<=gemCount;i++){
            gem_CP[i]=new Element( i , (float)gemsATK[i-1]/(float)gemsWeight[i-1] );
        }
        /*2.依照單位重量攻擊力排序*/
        java.util.Arrays.sort(gem_CP);
        Gem_ATK = new int[gemCount+1];
        Gem_Weight = new int[gemCount+1];
        Gem_ATK[0]=0;
        Gem_Weight[0]=0;
        Gem_in_Bag[0]=0;
        for(int i=1;i<= gemCount;i++){
            Gem_ATK[i] = gemsATK[ gem_CP[i].id-1];
            Gem_Weight[i]=gemsWeight[ (gem_CP[i].id) -1];
            Gem_in_Bag[i]=0;
        }
        /*回溯法開始*/
        Backtracking(1);
        /*回傳最佳攻擊*/
        return Best_ATK;
    }
    //寶石的單位重量
    public static class Element implements Comparable{
        int id;//寶石編號
        float CP;
        Element(int id, float CP){
            this.id=id;
            this.CP=CP;
        }
        @Override
        public int compareTo(Object o) {
            float x = ((Element)o).CP;
            if( CP > x )
                return -1;
            if( CP == x )
                return 0;
            return 1;
        }
    }
    private void Backtracking(int point){
        //葉片走到底
        if( point > Gem_Count ){
            if ( Now_ATK > Best_ATK)
                Best_ATK =  Now_ATK;
            return;
        }
        //拿取
        if( Now_Weight + Gem_Weight[point] <= Bag_size ){
            Gem_in_Bag[point] = 1;
            Now_Weight += Gem_Weight[ point ];
            Now_ATK += Gem_ATK [ point ];
            Backtracking(point+1 );
            Now_Weight -= Gem_Weight[ point ];
            Now_ATK -= Gem_ATK [ point ];
        }
        //不拿（跳過）
        if ( bound( point+1 ) > Best_ATK ){
            Gem_in_Bag[ point ] = 0 ;
            Backtracking( point+1 );
        }
    }
    //上界函数
    private double bound(int i){
        double cleft=Bag_size-Now_Weight;
        double bound= Now_ATK;
        while(i<=Gem_Count&&Gem_Weight[i]<=cleft){
            cleft-=Gem_Weight[i];
            bound+=Gem_ATK[i];
            i++;
        }
        if(i<=Gem_Count){
            bound+=Gem_ATK[i]*cleft/Gem_Weight[i];
        }
        return bound;
    }
}
