import com.sun.org.apache.xalan.internal.xsltc.dom.MultiValuedNodeHeapIterator;

import java.util.Stack;

/**
 * 作業03B - 背包問題(BranchAndBound)
 */
public class BinaryKnapsackWithBranchAndBound {
    class HeapNode {
        float unbound;
        int value;
        int weight;
        int level;
        HeapNode() {
        }
    }
    private int Gem_Count;      //寶石數量
    private int[] Gem_ATK;      //每顆寶石的攻擊力
    private int[] Gem_Weight;   //每顆寶石的重量
    private int Best_ATK;       //最佳攻擊力
    private int Bag_size;       //包包大小
    private int Now_Weight;     //當前重量
    private int Now_ATK;        //當前攻擊力
    private Stack<HeapNode> heap;

    public int solve(int gemCount, int[] gemsATK, int[] gemsWeight, int knapsackMaxWeight) {
        /*初始化各數據*/
        heap = new Stack<HeapNode>();
        Now_Weight = 0;
        Now_ATK = 0;
        Best_ATK = 0;
        Bag_size = knapsackMaxWeight;
        Gem_Count = gemCount;
        Element[] gem_CP = new Element[gemCount + 1];
        /*1.先將物品以單位重量(ATK/Weight)進行排序*/
        gem_CP[gemCount] = new Element( 0 , 0 );
        for(int i=0;i<gemCount;i++){
            gem_CP[i]=new Element( i , (float)gemsATK[i]/(float)gemsWeight[i] );
        }
        /*依照單位重量攻擊力排序*/
        java.util.Arrays.sort(gem_CP);
        Gem_ATK = new int[gemCount+1];
        Gem_Weight = new int[gemCount+1];
        Gem_ATK[gemCount]=0;
        Gem_Weight[gemCount]=0;
        for(int i=0;i< gemCount;i++){
            Gem_ATK[i] = gemsATK[ gem_CP[i].id];
            Gem_Weight[i]=gemsWeight[ (gem_CP[i].id) ];
        }
        /*排序結束*/
        return knapsack();
    }
    //寶石的單位重量
    public static class Element implements Comparable{
        int id;//寶石編號
        float CP;
        Element(int id, float CP){
            this.id=id;
            this.CP=CP;
        }
        @Override
        public int compareTo(Object o) {
            float x = ((Element)o).CP;
            if( CP > x )
                return -1;
            if( CP == x )
                return 0;
            return 1;
        }
    }
    //上界函数
    private float bound(int i){
        float Weight = Bag_size-Now_Weight;
        float bound = Now_ATK;
        while( (i < Gem_Count) && (Gem_Weight[i] <= Weight ) ){
            Weight -= Gem_Weight[i];
            bound += Gem_ATK[i];
            i++;
        }
        if( i < Gem_Count ){
            bound += Gem_ATK[i] * Weight / Gem_Weight[i];
        }
        return bound;
    }
    //加入節點
    private void addLiveNode(float upper, int c_value, int c_weight, int level) {
        HeapNode node = new HeapNode();
        node.unbound = upper;
        node.value = c_value;
        node.weight = c_weight;
        node.level = level;
        if (level <= Gem_Count)
            heap.push(node);
    }
    private int knapsack() {
        int i = 0;
        float upbound = bound(i);

        while (true)
        {
            double wt = Now_Weight + Gem_Weight[i];
            if (wt <= Bag_size)
            {
                if (Now_ATK + Gem_ATK[i] > Best_ATK)
                    Best_ATK = Now_ATK + Gem_ATK[i];
                addLiveNode(upbound, Now_ATK + Gem_ATK[i], Now_Weight + Gem_Weight[i],
                        i + 1);
            }
            upbound = bound(i + 1);
            if (upbound >= Best_ATK)
                addLiveNode(upbound, Now_ATK, Now_Weight, i + 1);
            if (heap.empty())
                return Best_ATK;
            HeapNode node = heap.peek();

            heap.pop();

            Now_Weight = node.weight;
            Now_ATK = node.value;
            upbound = node.unbound;
            i = node.level;
        }
    }
}
