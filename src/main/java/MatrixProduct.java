/**
 * 作業01 -- 矩陣相乘 (使用 Strassen's Algorithm)
 */
public class MatrixProduct {

    /**
     * 回傳 A * B，使用 Strassen's Algorithm
     *
     * @param A 矩陣 (m * n)
     * @param B 矩陣 (n * k)
     * @return C 為 A * B 的結果 (m * k)
     */
    public int[][] solve(int[][] A, int[][] B) {

        int m=A.length,n=A[0].length,p=B[0].length;
        int count=0,b=1,i,j,newsize;
        //Check if m>n or n>p cause a[m*n] b[n*p] so final size will be m*p
        if(m>n && m>p) {
            newsize=m;
        }
        else if(n>p&&n>m) {
            newsize=n;
        }
        else {
            newsize=p;
        }
        for(i=1;i<newsize;i++) {
            b*=2;
            if(b==newsize)
                break;
        }
        if(b==newsize) {
            newsize=b;
        }
        else {
            while (newsize != 0) {
                newsize = newsize / 2;
                count++;
            }
            newsize =  (int)Math.pow(2,count);
        }
        int [][] new_A = new int [newsize][newsize];
        int [][] new_B = new int [newsize][newsize];
        //define new arrayA to put just right size from what we get from newsize
        for( i=0;i<m;i++) {
            for( j=0;j<n;j++) {
                new_A[i][j] = A[i][j];
            }
        }
        //define new arrayB to put just right size from what we get from newsize
        for( i=0;i<n;i++) {
            for( j=0;j<p;j++) {
                new_B[i][j]=B[i][j];
            }
        }
        //Put final answer -- ans_C from algorithm from stra()
        int[][] ans_C = stra(new_A,new_B);

        //define new array C and use loop to put ans_C to C
        int[][] C = new int[m][p];
        for(i=0;i<m;i++) {
            for(j=0;j<p;j++) {
                C[i][j] = ans_C[i][j];
            }
        }
        return C;
    }

    public int[][]stra(int[][]A,int[][]B) {
        int n = A.length;
        int checksize = n/2;

        if(checksize == 0) {
            return MulMatrix(A,B);
        }
        else {
            //Defining new matrices
            int[][] a11 = new int[checksize][checksize];
            int[][] a12 = new int[checksize][checksize];
            int[][] a21 = new int[checksize][checksize];
            int[][] a22 = new int[checksize][checksize];

            int[][] b11 = new int[checksize][checksize];
            int[][] b12 = new int[checksize][checksize];
            int[][] b21 = new int[checksize][checksize];
            int[][] b22 = new int[checksize][checksize];

            int[][] aResult ;
            int[][] bResult;


            //Split matrices into one by one
            for (int i = 0; i < checksize; i++) {
                for (int j = 0; j < checksize; j++) {
                    a11[i][j] = A[i][j];
                    a12[i][j] = A[i][j + checksize];
                    a21[i][j] = A[i + checksize][j];
                    a22[i][j] = A[i + checksize][j + checksize];

                    b11[i][j] = B[i][j];
                    b12[i][j] = B[i][j + checksize];
                    b21[i][j] = B[i + checksize][j];
                    b22[i][j] = B[i + checksize][j + checksize];
                }
            }

            //Algorithm starts

            //p1
            aResult = add(a11, a22);//a11+a22
            bResult = add(b11, b22);//b11+b22
            int[][] p1 = MulMatrix(aResult, bResult);// P1 = (A11+A22) * (B11+B22)

            //p2
            aResult = add(a21, a22); // a21 + a22
            int[][] p2 = MulMatrix(aResult, b11); // p2 = (a21+a22) * (b11)
            //p3
            bResult = subtract(b12, b22); // b12 - b22
            int[][] p3 = MulMatrix(a11, bResult);// p3 = (a11) * (b12 - b22)

            //p4
            bResult = subtract(b21, b11); // b21 - b11
            int[][] p4 = MulMatrix(a22, bResult);// p4 = (a22) * (b21 - b11)

            //P5
            aResult = add(a11, a12); // a11 + a12
            int[][] p5 =MulMatrix(aResult, b22);// p5 = (a11+a12) * (b22)

            //P6
            aResult = subtract(a21, a11); // a21 - a11
            bResult = add(b11, b12); // b11 + b12
            int[][] p6 = MulMatrix(aResult, bResult); // p6 = (a21-a11) * (b11+b12)

            //P7
            aResult = subtract(a12, a22); // a12 - a22
            bResult = add(b21, b22); // b21 + b22
            int[][] p7 =MulMatrix(aResult, bResult); // p7 = (a12-a22) * (b21+b22)


            //C11 C12 C21 C22
            aResult = add(p1, p4); // p1 + p4
            bResult = add(aResult, p7); // p1 + p4 + p7
            int[][] c11 = subtract(bResult, p5); // c11 = p1 + p4 - p5 + p7

            int[][] c12 = add(p3, p5); // c12 = p3 + p5

            int[][] c21 = add(p2, p4); // c21 = p2 + p4

            aResult = add(p1, p3); // p1 + p3
            bResult = add(aResult, p6); // p1 + p3 + p6
            int[][] c22 = subtract(bResult, p2); // c22 = p1 + p3 - p2 + p6


            // Grouping the results obtained in a single matrix:
            int[][] C = new int[n][n];
            for (int i = 0; i < checksize; i++) {
                for (int j = 0; j < checksize; j++) {
                    C[i][j] = c11[i][j];
                    C[i][j + checksize] = c12[i][j];
                    C[i + checksize][j] = c21[i][j];
                    C[i + checksize][j + checksize] = c22[i][j];
                }
            }
            return C;
        }



    }


    public static int[][] MulMatrix(int[][] A,int[][] B)
    {
        int n =A.length;

        int[][] C = new int[n][n];

        for(int i = 0;i<n;i++) {
            for(int j=0;j<n;j++) {
                for(int k=0;k<n;k++) {
                    C[i][j]+=A[i][k]*B[k][j];
                }
            }
        }
        return C;
    }
    private static int[][] subtract(int[][] A, int[][] B)
    {
        int n = A.length;
        int[][] C = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                C[i][j] = A[i][j] - B[i][j];
            }
        }
        return C;
    }
    private static int[][] add(int[][] A, int[][] B) {
        int n = A.length;
        int[][] C = new int[n][n];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                C[i][j] = A[i][j] + B[i][j];
            }
        }
        return C;
    }

}