/**
 * 作業00 -- 找最大值
 */
public class FindMaxValue {

    /**
     * 從 numbers 中回傳最大值
     *
     * @param numbers 一堆數字
     * @return 最大值
     */
    public int solve(int[] numbers) {
        int maxValue = Integer.MIN_VALUE;
        for(int i=0 ; i <numbers.length ; i++){
            maxValue = Math.max(maxValue,numbers[i]);
        }

        return maxValue;
    }
}
