import org.junit.Test;

import static org.junit.Assert.*;

public class FindMaxValueTest {

    @Test(timeout = 10000)
    public void testcase01() {
        // Arrange
        FindMaxValue homework = new FindMaxValue();
        int[] numbers = new int[]{5, 1, 8, 4, 2};

        // Act
        int output = homework.solve(numbers);

        // Assert
        assertEquals(8, output);
    }

    @Test(timeout = 10000)
    public void testcase02() {
        // Arrange
        FindMaxValue homework = new FindMaxValue();
        int[] numbers = new int[]{10};

        // Act
        int output = homework.solve(numbers);

        // Assert
        assertEquals(10, output);
    }

    @Test(timeout = 10000)
    public void testcase03() {
        // Arrange
        FindMaxValue homework = new FindMaxValue();
        int[] numbers = new int[]{1, 2, 3, 4, 5};

        // Act
        int output = homework.solve(numbers);

        // Assert
        assertEquals(5, output);
    }

}