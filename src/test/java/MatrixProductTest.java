import org.junit.Test;

import static org.junit.Assert.assertArrayEquals;

public class MatrixProductTest {

    @Test(timeout = 10000)
    public void testcase01() {
        // Arrange
        MatrixProduct homework = new MatrixProduct();
        int[][] A = new int[][]{
                {5, 1},
                {8, 4}
        };
        int[][] B = new int[][]{
                {3, 2},
                {5, 9}
        };

        // Act
        int[][] output = homework.solve(A, B);
        int[][] ans = new int[][]{
                {20, 19},
                {44, 52}
        };
        // Assert
        assertArrayEquals(ans, output);
    }

    @Test(timeout = 10000)
    public void testcase02() {
        // Arrange
        MatrixProduct homework = new MatrixProduct();
        int[][] A = new int[][]{
                {5, 4, 8, -1},
                {0, 5, 10, -5}
        };
        int[][] B = new int[][]{
                {3, 7, 8},
                {4, 7, 20},
                {-51, -74, 10},
                {-8, 8, 0}
        };

        // Act
        int[][] output = homework.solve(A, B);
        int[][] ans = new int[][]{
                {-369, -537, 200},
                {-450, -745, 200}
        };

        // Assert
        assertArrayEquals(ans, output);
    }

    @Test(timeout = 10000)
    public void testcase03() {
        // Arrange
        MatrixProduct homework = new MatrixProduct();
        int[][] A = new int[][]{
                {5, 1, 8, 20},
                {7, 5, 10, -5},
                {6, 4, 80, 70},
                {8, 5, 10, 100},
        };
        int[][] B = new int[][]{
                {1, 0, 0, 0},
                {0, 1, 0, 0},
                {0, 0, 1, 0},
                {0, 0, 0, 1},
        };

        // Act
        int[][] output = homework.solve(A, B);
        int[][] ans = new int[][]{
                {5, 1, 8, 20},
                {7, 5, 10, -5},
                {6, 4, 80, 70},
                {8, 5, 10, 100},
        };

        // Assert
        assertArrayEquals(ans, output);
    }
}